//B1: Import Thư viện express
//import { Express } from "express";
const express = require("express");

//Import thư viện mongoose
var mongoose = require('mongoose');

const path = require('path');

//b2 khởi tạo app express
const app = new express();

//b3: khai báo cỗng để chạy api
const port = 8009;

mongoose.connect("mongodb://127.0.0.1:27017/CRUD_LuckyDice")
    .then(() => console.log("Connected to Mongo Successfully"))
    .catch(error => handleError(error));

//Khai báo các model
const userRouter = require("./app/routes/userRouter")
const diceHistoryRouter = require("./app/routes/diceHistoryRouter");
const prizeRouter = require("./app/routes/prizeRouter")
const voucherRouter = require("./app/routes/voucherRouter")
const prizeHistoryRouter = require("./app/routes/prizeHistoryRouter")
const voucherHistoryRouter = require("./app/routes/voucherHistoryRouter");
const prizeHistoryController = require("../controllers/prizeHistoryController")



//cấu hình để sử dụng json
app.use(express.json());

app.use(express.static(__dirname + "/app/views"));

//Middleware
//Middleware console log ra thời gian hiện tại 
app.use((req, res, next) => {
    console.log("Thời gian hiện tại:", new Date());

    next();
})

//middleware console log ra request method
app.use((req, res, next) => {
    console.log("Request method", req.method);

    next();
})

app.get("/", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/app/views/luckyDice.html"));

})

//Khai báo các api 
app.get("/random-number", (req, res) => { // min and max included 
    const rndInt = Math.floor(Math.random() * 6) + 1;
    try {
        return res.status(200).json({
            rndInt
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })

    }

})

app.get("/random-number", prizeHistoryRouter.getPrizeHistoryByUsername);

//Sử dụng router
app.use("/api/v1/users", userRouter);
app.use("/api/v1/dice-histories", diceHistoryRouter);
app.use("/api/v1/prizes", prizeRouter);
app.use("/api/v1/vouchers", voucherRouter);
app.use("/api/v1/prize-histories", prizeHistoryRouter);
app.use("/api/v1/voucher-histories", voucherHistoryRouter);


//b4: star app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})