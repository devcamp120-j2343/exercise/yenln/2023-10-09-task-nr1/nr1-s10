//B1 khai báo thư viện mongoose
const mongoose = require("mongoose")

//B2 Khai báo class Schema
const Schema = mongoose.Schema;

//B3 Khởi tạo schema với các thuộc tính của collection
const prizeSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false,
    },
    createdAt: {
        type: Date,
        default: Date.now(),
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
})

//B4: biên dịch schema thành model
module.exports = mongoose.model("prize", prizeSchema)