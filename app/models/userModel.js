//B1 khai báo thư viện mongoose
const mongoose = require("mongoose")

//B2 Khai báo class Schema
const Schema = mongoose.Schema;

//B3 Khởi tạo schema với các thuộc tính của collection
const userSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    username: {
        type: String,
        required: true,
        unique: true,
    },
    firstname: {
        type: String,
        required: true,
    },
    lastname: {
        type: String,
        required: true,
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    },

})

//B4: biên dịch schema thành model
module.exports = mongoose.model("user", userSchema)