//B1 khai báo thư viện mongoose
const mongoose = require("mongoose")

//B2 Khai báo class Schema
const Schema = mongoose.Schema;

//B3 Khởi tạo schema với các thuộc tính của collection
const prizeHistorySchema = new Schema({
    _id: mongoose.Types.ObjectId,
    user: [
        {
            type: mongoose.Types.ObjectId,
            ref: "user",
            required: true,
        }
    ],
    prize: [
        {
            type: mongoose.Types.ObjectId,
            ref: "prize",
            required: true,
        }
    ],
    createdAt: {
        type: Date,
        default: Date.now(),
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
})

//B4: biên dịch schema thành model
module.exports = mongoose.model("prizeHistory", prizeHistorySchema)