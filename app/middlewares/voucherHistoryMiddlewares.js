const getAllVoucherHistorysMiddlewares = (req, res, next) => {
    console.log("Get all VoucherHistorys Middleware");

    next();
}

const createVoucherHistorysMiddlewares = (req, res, next) => {
    console.log("Create VoucherHistorys Middleware");

    next();
}

const getDetailVoucherHistorysMiddlewares = (req, res, next) => {
    console.log("Get detail VoucherHistory Middleware");

    next();
}

const updateVoucherHistorysMiddlewares = (req, res, next) => {
    console.log("Update VoucherHistorys Middleware");

    next();
}

const deleteVoucherHistorysMiddlewares = (req, res, next) => {
    console.log("Delete VoucherHistorys Middleware");

    next();
}

module.exports = {
    getAllVoucherHistorysMiddlewares,
    createVoucherHistorysMiddlewares, 
    getDetailVoucherHistorysMiddlewares,
    updateVoucherHistorysMiddlewares,
    deleteVoucherHistorysMiddlewares
}