const getAllRandomNumberMiddlewares = (req, res, next) => {
    console.log("Get all RandomNumber Middleware");

    next();
}

const createRandomNumberMiddlewares = (req, res, next) => {
    console.log("Create RandomNumber Middleware");

    next();
}

const getDetailRandomNumberMiddlewares = (req, res, next) => {
    console.log("Get detail RandomNumber Middleware");

    next();
}

const updateRandomNumberMiddlewares = (req, res, next) => {
    console.log("Update RandomNumber Middleware");

    next();
}

const deleteRandomNumberMiddlewares = (req, res, next) => {
    console.log("Delete RandomNumber Middleware");

    next();
}

module.exports = {
    getAllRandomNumberMiddlewares,
    createRandomNumberMiddlewares, 
    getDetailRandomNumberMiddlewares,
    updateRandomNumberMiddlewares,
    deleteRandomNumberMiddlewares
}