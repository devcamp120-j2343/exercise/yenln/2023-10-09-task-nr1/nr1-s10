const getAllDiceHistorysMiddlewares = (req, res, next) => {
    console.log("Get all DiceHistorys Middleware");

    next();
}

const createDiceHistorysMiddlewares = (req, res, next) => {
    console.log("Create DiceHistorys Middleware");

    next();
}

const getDetailDiceHistorysMiddlewares = (req, res, next) => {
    console.log("Get detail DiceHistory Middleware");

    next();
}

const updateDiceHistorysMiddlewares = (req, res, next) => {
    console.log("Update DiceHistorys Middleware");

    next();
}

const deleteDiceHistorysMiddlewares = (req, res, next) => {
    console.log("Delete DiceHistorys Middleware");

    next();
}

module.exports = {
    getAllDiceHistorysMiddlewares,
    createDiceHistorysMiddlewares, 
    getDetailDiceHistorysMiddlewares,
    updateDiceHistorysMiddlewares,
    deleteDiceHistorysMiddlewares
}