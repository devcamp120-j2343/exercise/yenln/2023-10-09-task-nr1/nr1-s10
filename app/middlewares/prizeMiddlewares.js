const getAllPrizesMiddlewares = (req, res, next) => {
    console.log("Get all Prizes Middleware");

    next();
}

const createPrizesMiddlewares = (req, res, next) => {
    console.log("Create Prizes Middleware");

    next();
}

const getDetailPrizesMiddlewares = (req, res, next) => {
    console.log("Get detail Prize Middleware");

    next();
}

const updatePrizesMiddlewares = (req, res, next) => {
    console.log("Update Prizes Middleware");

    next();
}

const deletePrizesMiddlewares = (req, res, next) => {
    console.log("Delete Prizes Middleware");

    next();
}

module.exports = {
    getAllPrizesMiddlewares,
    createPrizesMiddlewares, 
    getDetailPrizesMiddlewares,
    updatePrizesMiddlewares,
    deletePrizesMiddlewares
}