const getAllPrizeHistorysMiddlewares = (req, res, next) => {
    console.log("Get all PrizeHistorys Middleware");

    next();
}

const createPrizeHistorysMiddlewares = (req, res, next) => {
    console.log("Create PrizeHistorys Middleware");

    next();
}

const getDetailPrizeHistorysMiddlewares = (req, res, next) => {
    console.log("Get detail PrizeHistory Middleware");

    next();
}

const updatePrizeHistorysMiddlewares = (req, res, next) => {
    console.log("Update PrizeHistorys Middleware");

    next();
}

const deletePrizeHistorysMiddlewares = (req, res, next) => {
    console.log("Delete PrizeHistorys Middleware");

    next();
}

module.exports = {
    getAllPrizeHistorysMiddlewares,
    createPrizeHistorysMiddlewares, 
    getDetailPrizeHistorysMiddlewares,
    updatePrizeHistorysMiddlewares,
    deletePrizeHistorysMiddlewares
}