//import { Express } from "express";
const express = require("express");

const RandomNumberMiddleware = require("../middlewares/RandomNumberMiddleware");

// const RandomNumberController = require("../controllers/RandomNumberController")

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});
 
router.get("/", RandomNumberMiddleware.getAllRandomNumberMiddlewares)

router.post("/", RandomNumberMiddleware.createRandomNumberMiddlewares)

router.get("/:RandomNumberId", RandomNumberMiddleware.getDetailRandomNumberMiddlewares)

router.put("/:RandomNumberId", RandomNumberMiddleware.updateRandomNumberMiddlewares)

router.delete("/:RandomNumberId", RandomNumberMiddleware.deleteRandomNumberMiddlewares)

module.exports = router;