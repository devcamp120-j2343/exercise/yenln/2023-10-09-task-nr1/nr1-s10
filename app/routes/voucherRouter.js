//import { Express } from "express";
const express = require("express");

const voucherMiddleware = require("../middlewares/voucherMiddlewares");

const voucherController = require("../controllers/voucherController")

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});

router.post("/", voucherMiddleware.createVouchersMiddlewares, voucherController.createVoucher)
 
router.get("/", voucherMiddleware.getAllVouchersMiddlewares, voucherController.getAllVoucher)

router.get("/:voucherId", voucherMiddleware.getDetailVouchersMiddlewares , voucherController.getVoucherById)

router.put("/:voucherId", voucherMiddleware.updateVouchersMiddlewares , voucherController.updateVoucherById)

router.delete("/:voucherId", voucherMiddleware.deleteVouchersMiddlewares, voucherController.deleteVoucherById)

module.exports = router;