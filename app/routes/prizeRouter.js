//import { Express } from "express";
const express = require("express");

const prizeMiddleware = require("../middlewares/prizeMiddlewares");

const prizeController = require("../controllers/prizeController")

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});

router.post("/", prizeMiddleware.createPrizesMiddlewares, prizeController.createPrize)
 
router.get("/", prizeMiddleware.getAllPrizesMiddlewares, prizeController.getAllPrize)

router.get("/:prizeId", prizeMiddleware.getDetailPrizesMiddlewares , prizeController.getPrizeById)

router.put("/:prizeId", prizeMiddleware.updatePrizesMiddlewares , prizeController.updatePrizeById)

router.delete("/:prizeId", prizeMiddleware.deletePrizesMiddlewares, prizeController.deletePrizeById)

module.exports = router;