//import { Express } from "express";
const express = require("express");

const prizeHistoryMiddleware = require("../middlewares/prizeHistoryMiddlewares");

const prizeHistoryController = require("../controllers/prizeHistoryController")

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});

router.post("/", prizeHistoryMiddleware.createPrizeHistorysMiddlewares, prizeHistoryController.createPrizeHistory)
 
router.get("/", prizeHistoryMiddleware.getAllPrizeHistorysMiddlewares, prizeHistoryController.getAllPrizeHistory)

router.get("/:prizeHistoryId", prizeHistoryMiddleware.getDetailPrizeHistorysMiddlewares , prizeHistoryController.getPrizeHistoryById)

router.put("/:prizeHistoryId", prizeHistoryMiddleware.updatePrizeHistorysMiddlewares , prizeHistoryController.updatePrizeHistoryById)

router.delete("/:prizeHistoryId", prizeHistoryMiddleware.deletePrizeHistorysMiddlewares, prizeHistoryController.deletePrizeHistoryById)

module.exports = router;