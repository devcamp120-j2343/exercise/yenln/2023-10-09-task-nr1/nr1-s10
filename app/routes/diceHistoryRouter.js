//import { Express } from "express";
const express = require("express");

const DiceHistoryMiddleware = require("../middlewares/diceHistoryMiddlewares");

const DiceHistoryController = require("../controllers/diceHistoryController")

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});
router.post("/", DiceHistoryMiddleware.createDiceHistorysMiddlewares, DiceHistoryController.createDiceHistory)
 
router.get("/", DiceHistoryMiddleware.getAllDiceHistorysMiddlewares, DiceHistoryController.getAllDiceHistory)

router.get("/:diceHistoryId", DiceHistoryMiddleware.getDetailDiceHistorysMiddlewares, DiceHistoryController.getDiceHistoryById)

router.put("/:diceHistoryId", DiceHistoryMiddleware.updateDiceHistorysMiddlewares, DiceHistoryController.updateDiceHistoryById)

router.delete("/:diceHistoryId", DiceHistoryMiddleware.deleteDiceHistorysMiddlewares, DiceHistoryController.deletediceHistoryById)

module.exports = router;