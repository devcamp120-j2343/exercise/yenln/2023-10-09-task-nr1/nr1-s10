//import { Express } from "express";
const express = require("express");

const userMiddleware = require("../middlewares/userMiddlewares");

const userController = require("../controllers/userController")

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});

router.post("/", userMiddleware.createUsersMiddlewares, userController.createUser)
 
router.get("/", userMiddleware.getAllUsersMiddlewares, userController.getAllUser)

router.get("/:userId", userMiddleware.getDetailUsersMiddlewares , userController.getUserById)

router.put("/:userId", userMiddleware.updateUsersMiddlewares , userController.updateUserById)

router.delete("/:userId", userMiddleware.deleteUsersMiddlewares, userController.deleteUserById)

module.exports = router;