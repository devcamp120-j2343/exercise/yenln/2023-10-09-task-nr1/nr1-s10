//import { Express } from "express";
const express = require("express");

const voucherHistoryMiddleware = require("../middlewares/voucherHistoryMiddlewares");

const voucherHistoryController = require("../controllers/voucherHistoryController")

const router = express.Router();

router.use((req, res, next) => {
    console.log("Request URL course: ", req.url);

    next();
});

router.post("/", voucherHistoryMiddleware.createVoucherHistorysMiddlewares, voucherHistoryController.createVoucherHistory)
 
router.get("/", voucherHistoryMiddleware.getAllVoucherHistorysMiddlewares, voucherHistoryController.getAllVoucherHistory)

router.get("/:voucherHistoryId", voucherHistoryMiddleware.getDetailVoucherHistorysMiddlewares , voucherHistoryController.getVoucherHistoryById)

router.put("/:voucherHistoryId", voucherHistoryMiddleware.updateVoucherHistorysMiddlewares , voucherHistoryController.updateVoucherHistoryById)

router.delete("/:voucherHistoryId", voucherHistoryMiddleware.deleteVoucherHistorysMiddlewares, voucherHistoryController.deleteVoucherHistoryById)

module.exports = router;