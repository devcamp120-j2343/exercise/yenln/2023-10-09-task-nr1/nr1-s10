const mongoose = require("mongoose");

const voucherModel = require("../models/voucherModel")
const voucherRouter = require("../routes/voucherRouter");

const createVoucher = async (req, res) => {
    //Thao tác với CSDL
    const {
        code,
        discount,
        note,
        createdAt,
        updatedAt
    } = req.body;

    if (!code) {
        return res.status(400).json({
            message: "code khong hop le",
        });
    }
    if (!discount) {
        return res.status(400).json({
            message: "discount khong hop le",
        });
    }

    try {
        const newVoucher = {
            _id: new mongoose.Types.ObjectId(),
            code,
            discount,
            note,
            createdAt,
            updatedAt
        }

        const createNewVoucher = await voucherModel.create(newVoucher);
        return res.status(201).json({
            status: "Create new voucher sucessfully",
            data: createNewVoucher,
        })
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

const getAllVoucher = async (req, res) => {
    const result = await voucherModel.find();

    try {
        if (!result && result.length > 0) {
            return res.status(201).json({
                status: "Get all vouchers sucessfully",
                data: result
            })
        }
        else {
            return res.status(404).json({
                status: "Not found any voucher",
                data: result
            })
        }

    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

const getVoucherById = async (req, res) => {
    var voucherId = req.params.voucherId;

    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucherId is invalid!"
        })
    }

    const result = await voucherModel.findById(voucherId);

    try {
        if (!result) {
            return res.status(404).json({
                status: "Not found detail of this voucher",
                data: result
            })
        }
        else {
            return res.status(201).json({
                status: `Get voucher ${voucherId} sucessfully`,
                data: result
            })

        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }


}

const updateVoucherById = async (req, res) => {
    //B1: thu thập dữ liệu
    const voucherId = req.params.voucherId;

    const {
        code,
        discount,
        note,
        createdAt,
        updatedAt
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucherId is invalid!"
        })
    }

    if (!code) {
        return res.status(400).json({
            message: "code khong hop le",
        });
    }
    if (!discount) {
        return res.status(400).json({
            message: "discount khong hop le",
        });
    }

    //B3: thực thi model
    try {
        let updatedVoucher = {

        }

        if (code) {
            updatedVoucher.code = code;
        }
        if (discount) {
            updatedVoucher.discount = discount;
        }
        if (note) {
            updatedVoucher.note = note;
        }
        if (createdAt) {
            updatedVoucher.createdAt = createdAt;
        }
        if (updatedAt) {
            updatedVoucher.updatedAt = updatedAt;
        }


        const result = await voucherModel.findByIdAndUpdate(
            voucherId,
            updatedVoucher
        );

        if (result) {
            return res.status(200).json({
                status: `Update voucher ${voucherId} sucessfully`,
                data: updatedVoucher
            })
        } else {
            return res.status(404).json({
                status: "Not found any voucher"
            })
        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deleteVoucherById = async (req, res) => {
    //B1: Thu thập dữ liệu
    var voucherId = req.params.voucherId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucherId is invalid!"
        })
    }

    try {
        const deletedVoucher = await voucherModel.findByIdAndDelete(voucherId);

        if (deletedVoucher) {
            return res.status(200).json({
                status: `Delete voucher ${voucherId} sucessfully`,
                data: deletedVoucher
            })
        } else {
            return res.status(404).json({
                status: "Not found any voucher"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}
