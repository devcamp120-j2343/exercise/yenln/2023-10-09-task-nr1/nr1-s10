const mongoose = require("mongoose");

const voucherHistoryModel = require("../models/voucherHistoryModel")
const voucherHistoryRouter = require("../routes/voucherHistoryRouter");

const createVoucherHistory = async (req, res) => {
    //Thao tác với CSDL
    const {
        user,
        voucher,
        createdAt,
        updatedAt
    } = req.body;

    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            status: "Bad request",
            message: "user is invalid!"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(voucher)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Voucher is invalid!"
        })
    }

    const newVoucherHistory = {
        _id: new mongoose.Types.ObjectId(),
        user,
        voucher,
        createdAt,
        updatedAt
    }

    voucherHistoryModel.create(newVoucherHistory)
        .then((data) => {
            return res.status(201).json({
                status: "Create new VoucherHistory sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getAllVoucherHistory = async (req, res) => {
    const result = await voucherHistoryModel.find();
    try {
        if (!result && result.length < 0) {
            return res.status(404).json({
                status: "Not found any VoucherHistory",
                result
            })
        }
        else {
            return res.status(201).json({
                status: "Get all VoucherHistorys sucessfully",
                result
            })

        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

const getVoucherHistoryById = async (req, res) => {
    var voucherHistoryId = req.params.voucherHistoryId;

    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucherHistoryId is invalid!"
        })
    }

    const result = await voucherHistoryModel.findById(voucherHistoryId);

    try {
        if (!result) {
            return res.status(404).json({
                status: "Not found detail of this VoucherHistory",
                data: result
            })
        }
        else {
            return res.status(201).json({
                status: `Get VoucherHistory ${voucherHistoryId} sucessfully`,
                data: result
            })

        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }


}

const updateVoucherHistoryById = async (req, res) => {
    //B1: thu thập dữ liệu
    const voucherHistoryId = req.params.voucherHistoryId;

    const {
        user,
        voucher,
        createdAt,
        updatedAt
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            status: "Bad request",
            message: "user is invalid!"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(voucher)) {
        return res.status(400).json({
            status: "Bad request",
            message: "voucher is invalid!"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "VoucherHistoryId is invalid!"
        })
    }

    //B3: thực thi model
    try {
        let updatedVoucherHistory = {

        }

        if (user) {
            updatedVoucherHistory.user = user;
        }
        if (voucher) {
            updatedVoucherHistory.voucher = voucher;
        }
        if (createdAt) {
            updatedVoucherHistory.createdAt = createdAt;
        }
        if (updatedAt) {
            updatedVoucherHistory.updatedAt = updatedAt;
        }

        const result = await voucherHistoryModel.findByIdAndUpdate(
            voucherHistoryId,
            updatedVoucherHistory
        );

        if (result) {
            return res.status(200).json({
                status: `Update VoucherHistory ${voucherHistoryId} sucessfully`,
                data: updatedVoucherHistory
            })
        } else {
            return res.status(404).json({
                status: "Not found any VoucherHistory"
            })
        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deleteVoucherHistoryById = async (req, res) => {
    //B1: Thu thập dữ liệu
    var voucherHistoryId = req.params.voucherHistoryId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "VoucherHistoryId is invalid!"
        })
    }

    try {
        const deletedVoucherHistory = await voucherHistoryModel.findByIdAndDelete(voucherHistoryId);

        if (deletedVoucherHistory) {
            return res.status(200).json({
                status: `Delete VoucherHistory ${voucherHistoryId} sucessfully`,
                data: deletedVoucherHistory
            })
        } else {
            return res.status(404).json({
                status: "Not found any VoucherHistory"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    createVoucherHistory,
    getAllVoucherHistory,
    getVoucherHistoryById,
    updateVoucherHistoryById,
    deleteVoucherHistoryById
}
