const mongoose = require("mongoose");

const prizeModel = require("../models/prizeModel")
const prizeRouter = require("../routes/prizeRouter");

const createPrize = async (req, res) => {
    //Thao tác với CSDL
    const {
        name,
        description,
        createdAt,
        updatedAt,
    } = req.body;

    if (!name) {
        return res.status(400).json({
            message: "name khong hop le",
        });
    }
    try {
        const newPrize = {
            _id: new mongoose.Types.ObjectId(),
            name,
            description,
            createdAt,
            updatedAt,
        }

        const createNewPrize = await prizeModel.create(newPrize);
        return res.status(201).json({
            status: "Create new prize sucessfully",
            data: createNewPrize,
        })
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

const getAllPrize = async (req, res) => {
    const result = await prizeModel.find();

    try {
        if (!result && result.length > 0) {
            return res.status(201).json({
                status: "Get all prizes sucessfully",
                data: result
            })
        }
        else {
            return res.status(404).json({
                status: "Not found any prize",
                data: result
            })
        }

    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

const getPrizeById = async (req, res) => {
    var prizeId = req.params.prizeId;

    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "prizeId is invalid!"
        })
    }

    const result = await prizeModel.findById(prizeId);

    try {
        if (!result) {
            return res.status(404).json({
                status: "Not found detail of this prize",
                data: result
            })
        }
        else {
            return res.status(201).json({
                status: `Get prize ${prizeId} sucessfully`,
                data: result
            })

        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }


}

const updatePrizeById = async (req, res) => {
    //B1: thu thập dữ liệu
    const prizeId = req.params.prizeId;

    const {
        name,
        description,
        createdAt,
        updatedAt,
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "prizeId is invalid!"
        })
    }

    if (!name) {
        return res.status(400).json({
            message: "name khong hop le",
        });
    }

    //B3: thực thi model
    try {
        let updatedPrize = {

        }

        if (name) {
            updatedPrize.name = name;
        }
        if (description) {
            updatedPrize.description = description;
        }
        if (createdAt) {
            updatedPrize.createdAt = createdAt;
        }
        if (updatedAt) {
            updatedPrize.updatedAt = updatedAt;
        }

        const result = await prizeModel.findByIdAndUpdate(
            prizeId,
            updatedPrize
        );

        if (result) {
            return res.status(200).json({
                status: `Update prize ${prizeId} sucessfully`,
                data: updatedPrize
            })
        } else {
            return res.status(404).json({
                status: "Not found any prize"
            })
        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deletePrizeById = async (req, res) => {
    //B1: Thu thập dữ liệu
    var prizeId = req.params.prizeId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "prizeId is invalid!"
        })
    }

    try {
        const deletedprize = await prizeModel.findByIdAndDelete(prizeId);

        if (deletedprize) {
            return res.status(200).json({
                status: `Delete prize ${prizeId} sucessfully`,
                data: deletedprize
            })
        } else {
            return res.status(404).json({
                status: "Not found any prize"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    createPrize,
    getAllPrize,
    getPrizeById,
    updatePrizeById,
    deletePrizeById
}
