const mongoose = require("mongoose");

const diceHistoryModel = require("../models/diceHistoryModel")
const diceHistoryRouter = require("../routes/diceHistoryRouter");

const createDiceHistory = async (req, res) => {
    //Thao tác với CSDL
    const {
        user,
        dice,
        createdAt,
        updatedAt
    } = req.body;

    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            status: "Bad request",
            message: "user is invalid!"
        })
    }

    if (!dice || isNaN(dice)) {
        return res.status(400).json({
            message: "dice khong hop le",
        });
    }

    const newdiceHistory = {
        _id: new mongoose.Types.ObjectId(),
        user,
        dice,
        createdAt,
        updatedAt
    }

    diceHistoryModel.create(newdiceHistory)
        .then((data) => {
            return res.status(201).json({
                status: "Create new diceHistory sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getAllDiceHistory = async (req, res) => {
    const result = await diceHistoryModel.find();
    try {
        if (!result && result.length < 0) {
            return res.status(404).json({
                status: "Not found any diceHistory",
                result
            })
        }
        else {
            return res.status(201).json({
                status: "Get all diceHistorys sucessfully",
                result
            })

        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

const getDiceHistoryById = async (req, res) => {
    var diceHistoryId = req.params.diceHistoryId;

    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "diceHistoryId is invalid!"
        })
    }

    const result = await diceHistoryModel.findById(diceHistoryId);

    try {
        if (!result) {
            return res.status(404).json({
                status: "Not found detail of this diceHistory",
                data: result
            })
        }
        else {
            return res.status(201).json({
                status: `Get diceHistory ${diceHistoryId} sucessfully`,
                data: result
            })

        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }


}

const updateDiceHistoryById = async (req, res) => {
    //B1: thu thập dữ liệu
    const diceHistoryId = req.params.diceHistoryId;

    const {
        user,
        dice,
        createdAt,
        updatedAt
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            status: "Bad request",
            message: "user is invalid!"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "diceHistoryId is invalid!"
        })
    }


    if (!dice || isNaN(dice)) {
        return res.status(400).json({
            message: "dice khong hop le",
        });
    }


    //B3: thực thi model
    try {
        let updatedDiceHistory = {

        }

        if (user) {
            updatedDiceHistory.user = user;
        }
        if (dice) {
            updatedDiceHistory.dice = dice;
        }
        if (createdAt) {
            updatedDiceHistory.createdAt = createdAt;
        }
        if (updatedAt) {
            updatedDiceHistory.updatedAt = updatedAt;
        }

        const result = await diceHistoryModel.findByIdAndUpdate(
            diceHistoryId,
            updatedDiceHistory
        );

        if (result) {
            return res.status(200).json({
                status: `Update diceHistory ${diceHistoryId} sucessfully`,
                data: updatedDiceHistory
            })
        } else {
            return res.status(404).json({
                status: "Not found any diceHistory"
            })
        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deletediceHistoryById = async (req, res) => {
    //B1: Thu thập dữ liệu
    var diceHistoryId = req.params.diceHistoryId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "diceHistoryId is invalid!"
        })
    }

    try {
        const deleteddiceHistory = await diceHistoryModel.findByIdAndDelete(diceHistoryId);

        if (deleteddiceHistory) {
            return res.status(200).json({
                status: `Delete diceHistory ${diceHistoryId} sucessfully`,
                data: deleteddiceHistory
            })
        } else {
            return res.status(404).json({
                status: "Not found any diceHistory"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    updateDiceHistoryById,
    deletediceHistoryById
}
