const mongoose = require("mongoose");

const prizeHistoryModel = require("../models/prizeHistoryModel")
const prizeHistoryRouter = require("../routes/prizeHistoryRouter");

const createPrizeHistory = async (req, res) => {
    //Thao tác với CSDL
    const {
        user,
        prize,
        createdAt,
        updatedAt
    } = req.body;

    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            status: "Bad request",
            message: "user is invalid!"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(prize)) {
        return res.status(400).json({
            status: "Bad request",
            message: "prize is invalid!"
        })
    }

    const newPrizeHistory = {
        _id: new mongoose.Types.ObjectId(),
        user,
        prize,
        createdAt,
        updatedAt
    }

    prizeHistoryModel.create(newPrizeHistory)
        .then((data) => {
            return res.status(201).json({
                status: "Create new prizeHistory sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getAllPrizeHistory = async (req, res) => {
    const result = await prizeHistoryModel.find();
    try {
        if (!result && result.length < 0) {
            return res.status(404).json({
                status: "Not found any prizeHistory",
                result
            })
        }
        else {
            return res.status(201).json({
                status: "Get all prizeHistorys sucessfully",
                result
            })

        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}

const getPrizeHistoryById = async (req, res) => {
    var prizeHistoryId = req.params.prizeHistoryId;

    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "prizeHistoryId is invalid!"
        })
    }

    const result = await prizeHistoryModel.findById(prizeHistoryId);

    try {
        if (!result) {
            return res.status(404).json({
                status: "Not found detail of this prizeHistory",
                data: result
            })
        }
        else {
            return res.status(201).json({
                status: `Get prizeHistory ${prizeHistoryId} sucessfully`,
                data: result
            })

        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }


}

const updatePrizeHistoryById = async (req, res) => {
    //B1: thu thập dữ liệu
    const prizeHistoryId = req.params.prizeHistoryId;

    const {
        user,
        prize,
        createdAt,
        updatedAt
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(user)) {
        return res.status(400).json({
            status: "Bad request",
            message: "user is invalid!"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(prize)) {
        return res.status(400).json({
            status: "Bad request",
            message: "prize is invalid!"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "prizeHistoryId is invalid!"
        })
    }

    //B3: thực thi model
    try {
        let updatedPrizeHistory = {

        }

        if (user) {
            updatedPrizeHistory.user = user;
        }
        if (prize) {
            updatedPrizeHistory.prize = prize;
        }
        if (createdAt) {
            updatedPrizeHistory.createdAt = createdAt;
        }
        if (updatedAt) {
            updatedPrizeHistory.updatedAt = updatedAt;
        }

        const result = await prizeHistoryModel.findByIdAndUpdate(
            prizeHistoryId,
            updatedPrizeHistory
        );

        if (result) {
            return res.status(200).json({
                status: `Update prizeHistory ${prizeHistoryId} sucessfully`,
                data: updatedPrizeHistory
            })
        } else {
            return res.status(404).json({
                status: "Not found any prizeHistory"
            })
        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deletePrizeHistoryById = async (req, res) => {
    //B1: Thu thập dữ liệu
    var prizeHistoryId = req.params.prizeHistoryId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "prizeHistoryId is invalid!"
        })
    }

    try {
        const deletedprizeHistory = await prizeHistoryModel.findByIdAndDelete(prizeHistoryId);

        if (deletedprizeHistory) {
            return res.status(200).json({
                status: `Delete prizeHistory ${prizeHistoryId} sucessfully`,
                data: deletedprizeHistory
            })
        } else {
            return res.status(404).json({
                status: "Not found any prizeHistory"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const getPrizeHistoryByUsername = async (req, res) => {
    const username = req.query.username;
    if (!username) return res.status(400).json({ message: "Username invalid" });

    const user = await userModel.findOne({ username });
    if (!user) return res.status(200).json([]);

    const prizeHistory = await prizeHistoryModel.find({ user: user._id });
    if (!prizeHistory) return res.status(400).json({ message: "Bad request !" });

    return res.status(200).json(prizeHistory);
};

module.exports = {
    createPrizeHistory,
    getAllPrizeHistory,
    getPrizeHistoryById,
    updatePrizeHistoryById,
    deletePrizeHistoryById,
    getPrizeHistoryByUsername
}
