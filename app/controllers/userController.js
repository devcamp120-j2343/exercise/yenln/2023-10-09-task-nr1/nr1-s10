const mongoose = require("mongoose");

const userModel = require("../models/userModel")
const UserRouter = require("../routes/userRouter");

const createUser = async (req, res) => {
    //Thao tác với CSDL
    const {
        username,
        firstname,
        lastname,
        createdAt,
        updatedAt
    } = req.body;

    if (!username) {
        return res.status(400).json({
            message: "username khong hop le",
        });
    }
    if (!firstname) {
        return res.status(400).json({
            message: "firstname khong hop le",
        });
    }
    if (!lastname) {
        return res.status(400).json({
            message: "lastname khong hop le",
        });
    }

    const newUser = {
        _id: new mongoose.Types.ObjectId(),
        username,
        firstname,
        lastname,
        createdAt,
        updatedAt
    }

    userModel.create(newUser)
        .then((data) => {
            return res.status(201).json({
                status: "Create new User sucessfully",
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getAllUser = (req, res) => {
    userModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(201).json({
                    status: "Get all Users sucessfully",
                    data
                })
            }
            else {
                return res.status(404).json({
                    status: "Not found any User",
                    data
                })

            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        })

}

const getUserById = async (req, res) => {
    var userId = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is invalid!"
        })
    }

    const result = await userModel.findById(userId);

    try {
        if (!result) {
            return res.status(404).json({
                status: "Not found detail of this User",
                data: result
            })
        }
        else {
            return res.status(201).json({
                status: `Get User ${userId} sucessfully`,
                data: result
            })

        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }


}

const updateUserById = async (req, res) => {
    //B1: thu thập dữ liệu
    const userId = req.params.userId;

    const {
        username,
        firstname,
        lastname,
        createdAt,
        updatedAt
    } = req.body;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is invalid!"
        })
    }

    if (!username) {
        return res.status(400).json({
            message: "username khong hop le",
        });
    }
    if (!firstname) {
        return res.status(400).json({
            message: "firstname khong hop le",
        });
    }
    if (!lastname) {
        return res.status(400).json({
            message: "lastname khong hop le",
        });
    }

    //B3: thực thi model
    try {
        let updatedUser = {

        }

        if (username) {
            updatedUser.username = username;
        }
        if (firstname) {
            updatedUser.firstname = firstname;
        }
        if (lastname) {
            updatedUser.lastname = lastname;
        }
        if (createdAt) {
            updatedUser.createdAt = createdAt;
        }
        if (updatedAt) {
            updatedUser.updatedAt = updatedAt;
        }


        const result = await userModel.findByIdAndUpdate(
            userId,
            updatedUser
        );

        if (result) {
            return res.status(200).json({
                status: `Update User ${userId} sucessfully`,
                data: updatedUser
            })
        } else {
            return res.status(404).json({
                status: "Not found any User"
            })
        }
    }
    catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

const deleteUserById = async (req, res) => {
    //B1: Thu thập dữ liệu
    var userId = req.params.userId;

    //B2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is invalid!"
        })
    }

    try {
        const deletedUser = await userModel.findByIdAndDelete(userId);

        if (deletedUser) {
            return res.status(200).json({
                status: `Delete User ${userId} sucessfully`,
                data: deletedUser
            })
        } else {
            return res.status(404).json({
                status: "Not found any User"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
}
